
<!-- Formulaire de connexion -->

<?php 
        
        // En cas d'erreur, on affiche un message
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        //On se connecte a mysql
        require '../data.php';
        $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', $dbuser, $dbpassword );
         
        

        //  Récupération de l'utilisateur et de son pass hashé
        $req = $bdd->prepare('SELECT id, pass FROM membres WHERE pseudo = :pseudo');
        $req->execute(array(
        'pseudo' => $pseudo));
        $resultat = $req->fetch();

        // Comparaison du pass envoyé via le formulaire avec la base
        $isPasswordCorrect = password_verify($_POST['pass'], $resultat['pass']);

                if (!$resultat)
                {
                echo 'Mauvais identifiant ou mot de passe !';
                }
                else
                {
                if ($isPasswordCorrect) {
                        session_start();
                        $_SESSION['id'] = $resultat['id'];
                        $_SESSION['pseudo'] = $pseudo;
                        echo 'Vous êtes connecté !';
                }
                else {
                        echo 'Mauvais identifiant ou mot de passe !';
                }
                }
// Probleme de pseudo a regler, sa me redirige  vers ma page admin.php mais rien n'apparait dans la table membre phpmyadmin
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog from scrach</title>
    <link rel="stylesheet" href="style.css">

</head>
<body>
<header>


<!-- On indique au membre qu'il est connecté  -->
        <?php 
            if (isset($_SESSION['id']) AND isset($_SESSION['pseudo']))
            {
            echo 'Bonjour ' . $_SESSION['pseudo'];
            }
        ?>

<!-- Formulaire de connexion -->
 

        <form action="admin.php" method="POST">

                <input type="text" name="pseudo" placeholder="Votre adresse pseudo">
                <input type="text" name="email" placeholder="Votre adresse mail">
                <input type="password" name="mdp" placeholder="Votre mot de passe">
                <input type="submit" value="Valider" onclick="">

        </form>

<h1> Blog from scratch</h1>

<!-- Menu -->

        <nav class="menu">
                <a href="index.php?page=accueil">Accueil</a>
                <a href="index.php?page=blog">Les articles</a>
                <a href="admin.php?page=blog">Editer</a>
                <a href="admin.php?page=config">Créer</a>

        </nav>

<!-- Barre de recherche -->

        <div class="search">
        <form method="post" action="search.php3">

                Entrez un mot clé:<br>
                <input type="text" name="Mot" size="15">
                <input type="submit" value="Rechercher" alt="Lancer la recherche!">

        </form>
        </div>


<!-- Filtrer en fonction des categories -->

        <?php
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);

        
                // On se connecte à MySQL
                require '../data.php';
                $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', $dbuser, $dbpassword );

                $sql= "SELECT * FROM categories";

                try
                {
                        $stmt=$bdd->prepare($sql);
                        $stmt->execute();
                        $results=$stmt->fetchAll();
                }
                
                catch(Exeption $ex)
                {
                        echo($ex -> getMessage());
                }
  
        ?>

        
       <SELECT NAME="categories">
                <OPTION> --Selectionner vos Categories -- </OPTION>

                <?php foreach ($results as $output){?>
                <OPTION>
                <?php echo $output["category"];?>
                </OPTION>
                <?php 
                } 
                ?>
                
        </SELECT>


</header>
