<!-- Header -->
<?php include ('../includes/header.php')?>


<?php

    // En cas d'erreur, on affiche un message
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);


    try
    {
        // On se connecte à MySQL
        require '../data.php';
        $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', $dbuser, $dbpassword);
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    
       //On affiche l'article lorsque l'on clique sur lire la suite
    $pagearticle = $_GET['id'];

    $reponse = $bdd->query(' SELECT * FROM articles
    WHERE id = '. $pagearticle);

    $donnees = $reponse->fetch();
?>


    <p>
        <strong>Le titre de l'article : </strong> : <?php echo $donnees['title']; ?><br />
        <?php echo $donnees['content'];?><br />
        <img src= "<?php echo $donnees['image_url']; ?>" alt=""><br />
        <strong>La date de publication : </strong> : <?php echo $donnees['published_at']; ?><br />
        <strong>Le temps de lecture : </strong> : <?php echo $donnees['reading_time']; ?><br />
    </p>


<!-- Footer -->
<?php include ('../includes/footer.php')?>
