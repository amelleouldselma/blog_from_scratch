
<!-- Le header -->
<?php include ('../includes/header.php')?>

<h1>Nos articles:</h1>



<?php

    // En cas d'erreur, on affiche un message
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    try
    {
        // On se connecte à MySQL
        require '../data.php';
        $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8',$dbuser , $dbpassword);
    }
    catch(Exception $e)
    {
        // En cas d'erreur, on affiche un message et on arrête tout
        die('Erreur : '.$e->getMessage());
    }

    // Ma requete mysql
    $reponse = $bdd->query('SELECT articles.id, articles.title, articles.content, articles.image_url,articles.published_at,
    articles.reading_time, authors.firstname, authors.lastname
        FROM articles  
        JOIN  authors ON articles.author_id = authors.id 
        ORDER BY published_at ASC'
    ); 


    // On affiche chaque entrée une à une

    while ($donnees = $reponse->fetch())
    {
        
    ?>

        <p>
            <strong>Le titre de l'article : </strong> : <?php echo $donnees['title']; ?><br />
            <?php echo strip_tags(substr($donnees['content'], 0, 300));?><br />
            <img src= "<?php echo $donnees['image_url']; ?>" alt=""><br />
            <strong>La date de publication : </strong> : <?php echo $donnees['published_at']; ?><br />
            <strong>Le temps de lecture : </strong> : <?php echo $donnees['reading_time']; ?><br />
            <strong>L'auteur : </strong> : <?php echo $donnees['firstname'] . " " . $donnees['lastname']; ?><br />
            <a href="./article.php?id=<?php echo $donnees['id']?>">Lire la suite</a>

        
        <!-- Requete pour afficher les categories -->
        <strong>Categories : </strong> 
        </p>
        <?php 
            $articleID = $donnees['id'];
            $categories = $bdd->query('SELECT categories.category FROM articles_categories
            JOIN categories ON articles_categories.category_id=categories.id
            WHERE articles_categories.articles_id = '.$articleID.'');

        
        while($category = $categories->fetch ) //Si j'ajoute ls parenthèsesapres le fetch uniquement le premier article s'affiche, je n'arrive pas à faire apparaitre les categories.
        {
            echo $category['category']."</br> ";
        } 
    
    }

    $reponse->closeCursor(); // Termine le traitement de la requête


        ?>


<!-- Le footer -->
<?php include ('../includes/footer.php')?>

</body>
</html>
