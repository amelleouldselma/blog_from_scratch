<?php 

    // En cas d'erreur, on affiche un message
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

// Vérification de la validité des informations

        // Hachage du mot de passe
        $pass_hache = password_hash($_POST['pass'], PASSWORD_DEFAULT);

        //On se connecte a mysql
        require '../data.php';
        $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch;charset=utf8', $dbuser, $dbpassword );

        // Insertion
        $req = $bdd->prepare('INSERT INTO membres(pseudo, pass, email, date_inscription) 
                        VALUES(:pseudo, :pass, :email, CURDATE())');
        $req->execute(array(
        'pseudo' => $pseudo,
        'pass' => $pass_hache,
        'email' => $email));
?>



